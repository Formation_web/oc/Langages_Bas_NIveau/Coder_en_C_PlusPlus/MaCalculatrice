// MaCalculette.cpp : définit le point d'entrée pour l'application console.
//

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;


int main()
{
	// ======================== Initialisation ======================= //

	double a(0), b(0);
	
	int choixOperation(0), quitter(0) ;

	int const addition(1);
	int const soustraction(2);
	int const multiplication(3);
	int const division(4);

	
	
	cout << "##########################################################" << endl;
	cout << "Bienvenue dans mon premier programme en C++ : Calculatrice" << endl;
	cout << "##########################################################" << endl;
	
	

	while (quitter != 1) {

		cout << "==============================================================================" << endl;
		cout << "Quel operation souhaitez-vous effectuer ? " << endl;
		cout << "            --------------                " << endl;
		cout << "Entrez le chiffre correspondant a l'une des opérations ci-dessous : " << endl;
		cout << "==============================================================================" << endl;

		cout << "---------------------------------" << endl;
		cout << "1 ---> Addition <----------------" << endl;
		cout << "2 ---> Soustraction <------------" << endl;
		cout << "3 ---> Multiplication <----------" << endl;
		cout << "4 ---> Division <----------------" << endl;
		cout << "5 ---> Quitter <-----------------" << endl;
		cout << "---------------------------------" << endl;

		cin >> choixOperation; // L'utilisateur decide de l'opération à effectuer

		// ===================================================== //
		// ================= TRAITEMENT ======================== //
		// ===================================================== //

		if (choixOperation == 1) {
			cout << "======== ADDITION ========" << endl;
			cout << "      --------------      " << endl;

			// TODO : Remplacer ces 2 lignes par une fonction
			cout << "Saisissez le premier operande :" << endl;
			cin >> a; // Saisie utilisateur 
			cout << "Saisissez le second operande :" << endl;
			cin >> b; // Saisie utilisateur B

			// Traitement du calcul
			double const resultatAddition(a + b);
			
			// Affichage du resultat
			cout << a << " + " << b << " = " << resultatAddition << endl;
			
		}
		else if (choixOperation == 2) {
			cout << "========= SOUSTRACTION ==========" << endl;
			cout << "         --------------         " << endl;

			// TODO : Remplacer ces 2 lignes par une fonction
			cout << "Saisissez le premier operande :" << endl;
			cin >> a; // Saisie utilisateur 
			cout << "Saisissez le second operande :" << endl;
			cin >> b; // Saisie utilisateur B

			// Traitement du calcul
			double const resultatSoustraction(a - b);
			
			// Affichage du resultat
			cout << a << " - " << b << " = " << resultatSoustraction << endl;
		}
		else if (choixOperation == 3) {
			cout << "=========== MULTIPLICATION ============" << endl;
			cout << "           -----------------      " << endl;

			// TODO : Remplacer ces 2 lignes par une fonction
			cout << "Saisissez le premier operande :" << endl;
			cin >> a; // Saisie utilisateur 
			cout << "Saisissez le second operande :" << endl;
			cin >> b; // Saisie utilisateur B

			// Traitement du calcul
			double const resultatMultiplication(a * b);

			// Affichage du resultat
			cout << a << " x " << b << " = " << resultatMultiplication << endl;
		}
		else if (choixOperation == 4) {
			cout << "============= DVISION =============" << endl;
			cout << "           --------------      " << endl;

			// TODO : Remplacer ces 2 lignes par une fonction
			cout << "Saisissez le premier operande :" << endl;
			cin >> a; // Saisie utilisateur A

			cout << "Saisissez le second operande :" << endl;
			cin >> b; // Saisie utilisateur B

			// Traitement du calcul
			double const resultatDivision(a / b);

			// Affichage du resultat
			cout << a << " / " << b << " = " << resultatDivision << endl;

		}
		else if (choixOperation == 5) {
			quitter = 1;
			cout << "A tres bientot" << endl;
		}
		else {
			cout << "Je ne pas compris votre choix !!" << endl;
		}
	}


	system("PAUSE"); // Met en pause le programme afin de ne pas refermer de suite la console

    return 0; // Termine le programme
}

